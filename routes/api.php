<?php

use Illuminate\Support\Facades\Route;

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('/order-status', 'Api\V1\OrderStatusController@post')->name('api/v1/order-status');
});
